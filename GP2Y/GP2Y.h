#ifndef GP2Y_H_
#define GP2Y_H_

#include "stm32f10x_gpio.h"



class GP2Y {

private:

	GPIO_TypeDef * 	sensor_gpio;
	uint16_t		sensor_pin;

	GPIO_TypeDef * 	iled_gpio;
	uint16_t		iled_pin;


public:

	GP2Y();

	void 		init(void);
	float 		get_dust_value(void);
	uint16_t	get_adc_value(void);
	void		wait(void);
	uint16_t	filter(uint16_t adc_value);
	float 		adc_to_voltage(uint16_t adc_value);
	float		data_process(float);


};

#endif
