#include "GP2Y.h"
#include "bluepill.h"
#include "stm32f10x_adc.h"
#include "stm32f10x_gpio.h"

#define            COV_RATIO                            0.2            // ug/m3 / mv
#define            NO_DUST_VOLTAGE                      500            //mv


GP2Y::GP2Y() {

	sensor_gpio = GPIOA;
	sensor_pin = GPIO_Pin_0;

	iled_gpio = GPIOA;
	iled_pin = GPIO_Pin_8;

	init();

} //GP2Y()


void GP2Y::init(void) {

	//sensor_pin
	if (sensor_gpio == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	}
	else if (sensor_gpio == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	}
	else if (sensor_gpio == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}

	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure PA0 (ADC Channel6) as analog input -------------------------*/
	GPIO_InitStructure.GPIO_Pin = sensor_pin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(sensor_gpio, &GPIO_InitStructure);


	//iled_pin
	if (iled_gpio == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	}
	else if (iled_gpio == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	}
	else if (iled_gpio == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}

	/* Configure PA8 as digital output */
	GPIO_InitStructure.GPIO_Pin = iled_pin;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(iled_gpio, &GPIO_InitStructure);
	GPIO_ResetBits(iled_gpio, iled_pin);                                             //default low, ILED closed



	ADC_InitTypeDef ADC_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_AFIO, ENABLE);

	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfChannel = 1;
	ADC_Init(ADC1, &ADC_InitStructure);

	ADC_RegularChannelConfig(ADC1, ADC_Channel_0, 1, ADC_SampleTime_239Cycles5);

	ADC_Cmd(ADC1, ENABLE);

	ADC_ResetCalibration(ADC1);
	while(ADC_GetResetCalibrationStatus(ADC1));
	ADC_StartCalibration(ADC1);
	while(ADC_GetCalibrationStatus(ADC1));


} //init()


float GP2Y::get_dust_value(void) {

	float voltage;

	voltage = adc_to_voltage(get_adc_value());

	voltage = data_process(voltage);

	return voltage;
} //get_dust_value()


uint16_t GP2Y::get_adc_value(void) {

	GPIO_SetBits(iled_gpio, iled_pin);
	wait();

	ADC_SoftwareStartConvCmd(ADC1, ENABLE);

	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) != SET);

	ADC_ClearFlag(ADC1, ADC_FLAG_EOC);

	GPIO_ResetBits(iled_gpio, iled_pin); //default low, ILED off

	return filter(ADC_GetConversionValue(ADC1));
} //get_adc_value()


void GP2Y::wait(void) {
	delay_us(280);
} //wait()

uint16_t GP2Y::filter(uint16_t adc_value) {

	static uint16_t flag_first = 0, _buff[10], sum;
	const uint16_t _buff_max = 10;

	if(flag_first == 0) {
		flag_first = 1;

	    for(uint8_t i = 0, sum = 0; i < _buff_max; i++) {
	    	_buff[i] = adc_value;
	    	sum = sum + _buff[i];
	    }
	    return adc_value;
	} else {
		sum = sum - _buff[0];
	    for(uint8_t i = 0; i < (_buff_max - 1); i++) {
	    	_buff[i] = _buff[i + 1];
	    }
	    _buff[9] = adc_value;
	    sum += _buff[9];

	    return (uint16_t) sum / 10.0;
	}
} //filter()


float GP2Y::adc_to_voltage(uint16_t adc_value) {

	float temp;

	temp = (3300 / 4096.0) * adc_value * 11;

	return temp;
} //adc_to_voltage()

float GP2Y::data_process(float voltage) {

	if(voltage >= NO_DUST_VOLTAGE) {
		voltage -= NO_DUST_VOLTAGE;

		return (voltage * COV_RATIO);
	} else
		return 0;
} //data_process()
