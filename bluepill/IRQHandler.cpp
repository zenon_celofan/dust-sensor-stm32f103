#include "IRQHandler.h"
//#include "stm32f10x_exti.h"
#include "bluepill.h"

#include "serial2.h"



extern uint32_t milliseconds_after_reset;
//uint8_t rx_frame[RX_FRAME_SIZE];



void TIM4_IRQHandler(void) {

	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);

	milliseconds_after_reset++;

} //TIM4_IRQHandler


void EXTI15_10_IRQHandler(void) {

	//EXTI_ClearITPendingBit(EXTI_Line10);

} //EXTI15_10_IRQHandler();


void USART3_IRQHandler(void) {

} //USART3_IRQHandler()
